CREATE SCHEMA IF NOT EXISTS `A` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;

CREATE TABLE IF NOT EXISTS `A`.`questions` (
  `question_id` int NOT NULL AUTO_INCREMENT,
  `question` varchar(255) NOT NULL,
  PRIMARY KEY (question_id));

CREATE TABLE IF NOT EXISTS `A`.`answers` (
  `answer_id` int NOT NULL AUTO_INCREMENT,
  `correctness` TINYINT(1) NOT NULL ,
  `answer` varchar(255) NOT NULL ,
  `question_id` int NOT NULL,
  PRIMARY KEY (answer_id),
  INDEX `fk_question_id` (`question_id`),
  CONSTRAINT `fk_question_id` FOREIGN KEY (question_id) REFERENCES `A`.`questions` (`question_id`));

INSERT INTO `A`.`questions` (question_id, question)
VALUES (1, 'My parents bought their house ..... 1967.');
INSERT INTO `A`.`questions` (question_id, question)
VALUES (2, 'There''s somebody waiting ..... the bus stop.');
INSERT INTO `A`.`questions` (question_id, question)
VALUES (3, 'Gerhard has some nice pictures hanging ..... his office wall.');
INSERT INTO `A`.`questions` (question_id, question)
VALUES (4, 'Put the papers ..... the desk please.');
INSERT INTO `A`.`questions` (question_id, question)
VALUES (5, 'I''m moving to a new flat ..... 7 August.');
INSERT INTO `A`.`questions` (question_id, question)
VALUES (6, 'I often visit my parents ..... Christmas. ');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'in', '1');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'at', '1');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'on', '1');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'in', '2');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'at', '2');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'on', '2');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'in', '3');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'at', '3');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'on', '3');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'in', '4');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'at', '4');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'on', '4');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'in', '5');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'at', '5');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'on', '5');

INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'in', '6');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('1', 'at', '6');
INSERT INTO `A`.`answers` (correctness, answer, question_id)
VALUES ('0', 'on', '6');