package com.sergii.dictionary.service;

import com.sergii.dictionary.model.Question;
import com.sergii.dictionary.model.Result;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;

import java.util.List;

public interface QuestionService {
    @NotNull
    List<Question> getRandomQuestions(int bound);

    void verifyAnswer(int answerId, @NotNull List<Question> questionList, @NotNull Result result);

    @Nullable
    Question nextQuestion(@NotNull Result result, @NotNull List<Question> questionList);
}
