package com.sergii.dictionary.service;

import com.sergii.dictionary.dao.QuestionDao;
import com.sergii.dictionary.model.Answer;
import com.sergii.dictionary.model.Question;
import com.sergii.dictionary.model.Result;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class QuestionServiceImpl implements QuestionService {

    @Autowired
    private QuestionDao questionDao;

    @Override
    @Transactional
    @NotNull
    public List<Question> getRandomQuestions(int bound) {
        List<Question> questionList = questionDao.listQuestions();
        Collections.shuffle(questionList);
        return questionList.subList(0, bound);
    }

    public void verifyAnswer(int answerId, @NotNull List<Question> questionList, @NotNull Result result) {
        Set<Answer> answers = questionList.get(result.getNumberCurrentQuestion()).getAnswers();
        for (Answer answer : answers) {
            if (answer.getAnswerId() == answerId) {
                if (answer.isCorrectness()) {
                    result.incrementRightAnswer();
                }
            }
        }
    }

    @Override
    @Nullable
    public Question nextQuestion(@NotNull Result result, @NotNull List<Question> questionList) {
        if (result.getNumberCurrentQuestion() == questionList.size() - 1) {
            return null;
        }
        result.incrementCurrentQuestion();
        return questionList.get(result.getNumberCurrentQuestion());
    }
}