package com.sergii.dictionary.dao;

import com.sergii.dictionary.model.Question;

import java.util.List;

public interface QuestionDao {
    List<Question> listQuestions();
}
