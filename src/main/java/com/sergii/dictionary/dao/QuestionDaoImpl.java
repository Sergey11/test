package com.sergii.dictionary.dao;

import com.sergii.dictionary.model.Question;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class QuestionDaoImpl implements QuestionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @SuppressWarnings("unchecked")
    public List<Question> listQuestions() {
        return sessionFactory.getCurrentSession().createQuery("from Question").list();
    }
}