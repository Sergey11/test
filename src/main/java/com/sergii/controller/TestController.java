package com.sergii.controller;

import com.sergii.dictionary.model.Question;
import com.sergii.dictionary.model.Result;
import com.sergii.dictionary.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class TestController {

    @Autowired
    private QuestionService questionService;

    private static final int AMOUNT_QUESTION = 5;

    @ResponseBody
    @RequestMapping("/start")
    public Question getQuestion(HttpServletRequest request) {
        List<Question> questions = questionService.getRandomQuestions(AMOUNT_QUESTION);
        HttpSession httpSession = request.getSession();
        httpSession.setAttribute("randomQuestion", questions);
        httpSession.setAttribute("result", new Result(AMOUNT_QUESTION));
        return questions.get(0);
    }

    @ResponseBody
    @RequestMapping(value = "/question", method = RequestMethod.POST)
    @SuppressWarnings("unchecked")
    public Question getQuestion(@RequestParam int answerId, HttpServletRequest request) {
        HttpSession httpSession = request.getSession();
        List<Question> questionList = (List<Question>) httpSession.getAttribute("randomQuestion");
        Result result = (Result) httpSession.getAttribute("result");
        questionService.verifyAnswer(answerId, questionList, result);
        return questionService.nextQuestion(result, questionList);
    }

    @ResponseBody
    @RequestMapping(value = "/result", method = RequestMethod.POST)
    public Result getResult(HttpServletRequest request) {
        return (Result) request.getSession().getAttribute("result");

    }
}
