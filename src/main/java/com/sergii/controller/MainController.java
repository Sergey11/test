package com.sergii.controller;

import com.sergii.dictionary.service.QuestionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping("/")
public class MainController {

    @Autowired
    QuestionService questionService;

    @RequestMapping(value = {"/", "/welcome**"})
    public ModelAndView defaultPage() {
        ModelAndView model = new ModelAndView("test");
        model.addObject("welcome_message", "Welcome to test ");
        return model;
    }
}