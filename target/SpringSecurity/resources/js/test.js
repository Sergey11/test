function start() {
    $.ajax({
        type: "POST",
        url: "/start",
        success: function (question) {
            questionView(question);
            $('#result').empty();
            MyTimer.resetCountdown();
            MyTimer.Timer.toggle();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError);
        }
    });
}

function questionView(question) {
    var result = "<p><b>" + question.question + "</b></p><div id='variantAnswer'>";
    for (var i = 0; i < question.answers.length; i++) {
        result += "<input type='radio' name='answer' value='" + question.answers[i].answerId
        + "'/>" + question.answers[i].answer + "<br/>";
    }
    result += "<button id='next' disabled>Next</button>";
    $("#question").html(result);
    $("input[name='answer']").on('change', function () {
        $("#next").removeAttr('disabled');
    });
    $('#next').click(function () {
        MyTimer.resetCountdown();
        MyTimer.Timer.toggle();
        nextQuestion($('input[name="answer"]:checked', '#variantAnswer').val());
    });
}

function nextQuestion(answerId) {
    MyTimer.resetCountdown();
    MyTimer.Timer.toggle();
    var answer = {answerId: answerId};
    $.post("/question", answer, function (question) {
        if (question === "") {
            getResult();
        } else {
            questionView(question);
        }
    }).fail(function () {
        alert("error");
    });
}

function getResult() {
    MyTimer.resetCountdown();
    $.post("/result", function (result) {
        $('#result').html("<p>Your completed test with result "
        + result.amountRightAnswers + " from "
        + result.amountQuestions + "</p>");
        $('#countdown').empty();
        $('#question').empty();
    }).fail(function () {
        alert("error");
    });
}

var MyTimer = new (function () {
    var $countdown,
        incrementTime = 70,
        currentTime = 3000,
        updateTimer = function () {
            $countdown.html(formatTime(currentTime));
            if (currentTime == 0) {
                MyTimer.Timer.stop();
                nextQuestion(0);
                return;
            }
            currentTime -= incrementTime / 10;
            if (currentTime < 0) currentTime = 0;
        },
        init = function () {
            $countdown = $('#countdown');
            MyTimer.Timer = $.timer(updateTimer, incrementTime, false);
        };
    this.resetCountdown = function () {
        currentTime = 3000;
        this.Timer.stop().once();
    };
    $(init);
});

function pad(number, length) {
    var str = '' + number;
    while (str.length < length) {
        str = '0' + str;
    }
    return str;
}
function formatTime(time) {
    var min = parseInt(time / 6000),
        sec = parseInt(time / 100) - (min * 60),
        hundredths = pad(time - (sec * 100) - (min * 6000), 2);
    return (min > 0 ? pad(min, 2) : "00") + ":" + pad(sec, 2) + ":" + hundredths;
}

