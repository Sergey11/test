<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Main</title>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/test.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.timer.js"></script>
    <link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/test.css"/>
</head>
<body>
<div id="test">
<h1>${welcome_message}</h1>
    <button id="start" onclick="start()">Start test</button>
    <div id="result"></div>
    <div id="question"></div>
    <span id="countdown"></span>
</div>
</body>
</html>
